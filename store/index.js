window.rootUrl = "/api"
export const state = () => ({

  alert:{message:0,type:"success",countDown:3},
  token:null,
  feed:[],
  followers:[],
  followings:[],
  users:[]
})

export const mutations = {
  attempLogin(state, logindata) {
    state.token = logindata;
  },
  clearAlert(state)  {
    state.alert = {message:null,type:"success"};
  },
  setAlert(state, {message,type} ) {
    state.alert = {message, type,countDown:3}

  },
  updateFeed(state,postArr){
    state.feed =  postArr
  },
  setFollowers(state,postArr){
    state.followers = postArr;
  },
  setFollowings(state,postArr){
    state.followings = postArr;
  },
  setAllUsers(state,postArr){
    state.users = postArr;
  }
}
export const actions = {
  attempLogin({commit}, logindata) {
   return  fetch(window.rootUrl + "/member/login", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(logindata)
    })
      .then(response => response.json())
      .then((response) => {
        if (response.success) {
          commit("setAlert", {message:"basarılı",type:"success"})
            commit("attempLogin",response.data.token)
            return  true



        }
        else {
          commit("setAlert", {message:response,type:"warning"})
          return  false;
        }

      })


  },
  attempRegister({commit}, registerData) {
    return  fetch(window.rootUrl + "/member/", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(registerData)
    })
      .then(response => response.json())
      .then((response) => {
        if (response.success) {
          commit("setAlert", {message:"basarılı",type:"success"})
          return true;
        }
        else {
          commit("setAlert", {message:response,type:"warning"})
          return  false;
        }

      })


  },
  getFeed({commit,state,dispatch}){
    return  fetch(window.rootUrl + "/post/feed", {
      method: "GET",
      headers: {
        'Content-Type': 'application/json',
        'token':state.token
      }

    })
      .then(response => response.json())
      .then((response) => {
        if (response.success) {
             dispatch("getFollowers")
             commit("updateFeed",response.data)
            return  true

        }
        else {
          commit("setAlert", {message:response,type:"warning"})
          return  false;
        }

      })
  },
  getFollowers({commit,state}){
    return  fetch(window.rootUrl + "/member/follow/", {
      method: "GET",
      headers: {
        'Content-Type': 'application/json',
        'token':state.token
      }

    })
      .then(response => response.json())
      .then((response) => {
        if (response.success) {
          commit("setFollowers",response.data)
          return  true

        }
        else {
          commit("setAlert", {message:response,type:"warning"})
          return  false;
        }

      })
  },
  getFollowings({commit,state}){
    return  fetch(window.rootUrl + "/member/following/", {
      method: "GET",
      headers: {
        'Content-Type': 'application/json',
        'token':state.token
      }

    })
      .then(response => response.json())
      .then((response) => {
        if (response.success) {
          commit("setFollowings",response.data)
          return  true

        }
        else {
          commit("setAlert", {message:response,type:"warning"})
          return  false;
        }

      })
  },
  getAllUsers({commit,state,dispatch}){
    return  fetch(window.rootUrl + "/member/", {
      method: "GET",
      headers: {
        'Content-Type': 'application/json',
        'token':state.token
      }

    })
      .then(response => response.json())
      .then((response) => {
        if (response.success) {
          dispatch("getFollowers");
          dispatch("getFollowings")
          commit("setAllUsers",response.data)
          return  true

        }
        else {
          commit("setAlert", {message:response,type:"warning"})
          return  false;
        }

      })
  }
}

export const getters = {
  alert(state) {
    return state.alert;
  },
  token(state){
    return (!!state.token )
  },
  isFollowingMe(state){
    return ((userId) =>  {
     return (state.followers.filter(a=>a.userId==userId).length >0)
    })
  }, amIfollowing(state){
    return ((userId) =>  {
     return (state.followings.filter(a=>a.userId==userId).length >0)
    })
  }

}
